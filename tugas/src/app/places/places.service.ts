import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  _places: Place[] = [
    new Place(
      'p1',
      'Mayuh Futsal ',
      'Booking online lapangan futsal di Kota Purwokerto, Kami memiliki 4 lapangan futsal dengan area yang nyaman. Jadwal lapangan futsal kami dapat dilihat disini.',
      'https://cdn1-production-images-kly.akamaized.net/tahv2nYfyEdyKIsBCyEqb7Qn5IU=/1231x710/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/1804380/original/036302600_1513507715-Lapangan-Futsal1.jpg',
      192.29,

    ),
    new Place(
      'p2',
      'Lapangan 1',
      'Lapangan Rumput Sintetis',
      'https://tazvita.com/wp-content/uploads/2017/07/lapangan-futsal-e1500518630769.jpg',
      120000,

    ),
    new Place(
      'p3',
      'Lapangan 2',
      'Lapangan Rumput Sintetis',
      'https://tazvita.com/wp-content/uploads/2017/07/lapangan-futsal-e1500518630769.jpg',
      120000,

    ),
    new Place(
      'p4',
      'Lapangan 3',
      'Lapangan karpet vinyl',
      'https://ragasportflooring.co.id/wp-content/uploads/2020/08/biaya-pembuatan-lapangan-futsal.webp',
      150000,

    ),
    new Place(
      'p5',
      'Lapangan 4',
      'Lapangan karpet vinyl',
      'https://ragasportflooring.co.id/wp-content/uploads/2020/08/biaya-pembuatan-lapangan-futsal.webp',
      150000,

    ),
  ];

  get places() {
    return [...this._places];
  }

  constructor() { }

  getPlace(id: string) {
    return {
      ...this._places.find(p => {
        return p.id === id
      })
    };
  }
}
