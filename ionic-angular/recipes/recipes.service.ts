import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  recipes : Recipe[] = [
    {
      id : 'r1',
      title : 'Mendoan',
      imageUrl : 'https://cdn-brilio-net.akamaized.net/news/2019/10/29/173086/750xauto-5-resep-dan-cara-membuat-tempe-mendoan-enak-dan-gurih-191029y.jpg',
      ingredients : ['Bahan-Bahan:', '- Tempe iris tipis/khusus mendoan','- 6 sdm terigu','- 2 sdm tepung beras', '2 sdm tapioka',
      '- 1 batang daun bawang iris tipis', '- 200 ml air', '- Garam', '- Minyak untuk menggoreng', 'Bumbu halus:', '- 3 bawang putih', '- 1 sdm ketumbar', 'Cara memasak:', '- Campur bumbu halus dan bahan lainnya, aduk rata, kecuali tempe',
      '- Cicipi asinnya', '- Panaskan minyak agak banyak(sampai benar-benar panas) celupkan tempe ke adonan tepung, goreng sebentar/asal tepungnya matang', '- Angkat, sajikan dengan cabai rawit/sambal kecap',
      'Sambal Kecap:', '- Cabe rawit secukupnya iris tipis, tambahkan kecap manis secukupnya, aduk rata']
    },
    {
      id : 'r2',
      title : 'Perkedel',
      imageUrl : 'https://img-global.cpcdn.com/recipes/54b1c0525634fdb7/751x532cq70/perkedel-kentang-foto-resep-utama.jpg',
      ingredients : ['Bahan:', '- 1 kg kentang', '- Bawang goreng secukupnya', '- Daun prei secukupnya, iris halus', '- Garam secukupnya', '- Penyedap rasa secukupnya', '- Merica bubuk secukupnya',
      '- 1 butir telur (kocok lepas)', 'Bumbu halus:', '- 6 siung bawang merah', '- 4 siung bawang putih',
      'Cara membuat:',
      '- Kupas kentang, cuci bersih, potong-potong, lalu digoreng. Tiriskan kemudian uleg sampai halus',
      '- Campur kentang yang telah dihaluskan dengan bumbu halus, daun prei, garam, penyedap rasa, merica bubuk dan bawang goreng. Aduk rata, tes rasa.',
      '- Ambil sedikit adonan, bentuk bulat. Lalu pipihkan, lakukan sampai adonan habis.',
      '- Masukkan kentang yang sudah dibentuk, ke dalam telur yang sudah dikocok lepas, lalu digoreng hingga kuning kecokelatan.']
    },
  ];
  constructor() { }

  getAllRecipes(){
    return [...this.recipes]
  };

  getRecipe(recipeId: string){
    return {
      ...this.recipes.find(recipe => {
        return recipe.id === recipeId;
      })
    }
  }

  deleteRecipe(recipeId: string) {
   this.recipes = this.recipes.filter(recipe =>{
    return recipe.id !== recipeId;
   }); 
  }
}
