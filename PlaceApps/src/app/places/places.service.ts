import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  _places: Place[] = [
    new Place(
      'p1',
      'Ensuring Healthy Environments',
      'Where we work, play, live, learn and pray should help our health, not harm it',
      'https://www.heart.org/-/media/images/affiliates/fda/general-photos/father-helping-son-ride-bike.jpg?h=317&la=en&mw=600&w=475&hash=BB205444312E41C10BDF2D63318E3CE426557772',
      192.29,
      new Date('2019-01-01'),
      new Date('2019-12-31')
    ),
    new Place(
      'p2',
      'Strengthening the Economy',
      'A community\'s well-being is directly related to the health of its local economy',
      'https://www.heart.org/-/media/images/affiliates/fda/general-photos/man-riding-bike-to-work.jpg?h=333&la=en&mw=600&w=475&hash=B7D932604AF35513050FB981720CA8AE92A2B381',
      189.29,
      new Date('2019-01-01'),
      new Date('2019-12-03')
    ),
    new Place(
      'p3',
      'Improving Quality of Life',
      'Living healthy and free of disease makes a critical difference in quality of life',
      'https://www.heart.org/-/media/images/affiliates/fda/general-photos/family-eating-dinner.png?h=316&la=en&mw=600&w=475&hash=C33686ECEE49E10433ADBEFF12D89822CA804D8C',
      189.29,
      new Date('2019-01-01'),
      new Date('2019-11-11')
    )
  ];

  get places() {
    return [...this._places];
  }

  constructor() { }

  getPlace(id: string) {
    return {
      ...this._places.find(p => {
        return p.id === id
      })
    };
  }
}
